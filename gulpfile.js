'use strict';

const gulp = require('gulp');
const $ = require('gulp-load-plugins')();

let main_files = ['./src/**.js', './config/*.js', './*.js', './test/**.js'];

gulp.task('lint', () => {
	return gulp.src(main_files)
		.pipe($.eslint())
		.pipe($.eslint.format())
		.pipe($.eslint.failAfterError());
});

gulp.task('watch', () => {
	gulp.watch(main_files, ['lint']);
});

gulp.task('default', ['watch']);
