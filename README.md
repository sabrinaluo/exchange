# exchange-rate-worker
## Table of Content

## Start
```
npm start
```
This app will read `NODE_ENV`, if it is undefined, `./config/dev.json` is used by default.

### Run on production
```
export NODE_ENV=prod
npm start
```
OR
```
node index --NODE_ENV prod
```
Alias for variable is supported, and it is NOT case sensitive.
|env|alias|
|:---|:---|
|dev|develop|
|prod|production|

##Log
Use [bunyan](https://github.com/trentm/node-bunyan) as logger.
All the log stores in `./logs`
### Other Tools
- Beanstalkd Dashboard
- mongo-express
