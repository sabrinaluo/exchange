'use strict';

const jobType = 'exchange_rate';

function getExchangeRatePayload(from, to, success_times, fail_times) {
	return {
		type: jobType,
		payload: {
			from: from,
			to: to,
			success_times: success_times || 0,
			fail_times: fail_times || 0
		}
	};
}

module.exports = {
	getExchangeRatePayload
};
