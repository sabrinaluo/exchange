'use strict';
const Consumer = require('./src/consumer');
const Producer = require('./src/producer');
const util = require('./src/util');

const TUBE_NAME = 'sabrinaluo';
let producer = new Producer(TUBE_NAME);
let payload = util.getExchangeRatePayload('USD', 'HKD', 0, 0);
producer.put(payload, 5);
let worker = new Consumer(TUBE_NAME);
worker.start();
